# CKI Tools

All the command line tools the CKI team uses.

## Tests

To run the tests, just type `tox`. That should take care of installing
all dependencies needed for it. However, you have to make sure you
install the package `krb5-devel` (with `dnf`, `yum`, or similar).

You can also run tests in a podman container via

```shell
podman run --rm -it --volume .:/code --workdir /code registry.gitlab.com/cki-project/containers/python tox
```
## Optional dependencies

To use the `brew_trigger` module, use the `brew` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[brew]
```

To use the `kcidb` module, use the `kcidb` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[kcidb]
```

To use the `beaker-respin` CLI, use the `beaker_respin` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[beaker_respin]
```

## `shell-scripts/cki_deployment_acme.sh`

Update SSL certificates via dns-01 ACME challenges on Route 53, and deploy them via OpenShift routes.

```shell
Usage: cki_deployment_acme.sh [patch|cron]
```

In `cron` mode, the certificates are checked, updated via the certificate
authority if necessary, and deployed by patching OpenShift routes. In `patch`
mode, existing certificates are only deployed, and no certificate authority is
contacted.

Openshift routes that should be managed need to be annotated with
`cki-project.org/acme: certificate-name`. This [certificate name] normally
corresponds to the first domain name in a line of the `ACME_DOMAINS` variable.

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  annotations:
    cki-project.org/acme: cki-project.org
```

| Environment variable          | Type   | Required | Description                                                                                                      |
|-------------------------------|--------|----------|------------------------------------------------------------------------------------------------------------------|
| `IS_PRODUCTION`               | bool   | yes      | Contact either the production or staging endpoint of Let's Encrypt certificate authority.                        |
| `ACME_DOMAINS`                | string | yes      | Domain names for the certificates; certificates are issued per line for one or more space-separated domain names |
| `ACME_AWS_ACCESS_KEY_ID`      | string | yes      | Access key of the AWS service account that is used to modify the Route 53 zones                                  |
| `ACME_AWS_SECRET_ACCESS_KEY`  | string | yes      | Secret key of the AWS service account that is used to modify the Route 53 zones                                  |
| `ACME_BUCKET`                 | string | yes      | Deployment-all style bucket specification for the backup of registration data, keys and certificates             |
| `ACME_PASSWORD`               | string | yes      | Password for the encryption of the backup tarballs                                                               |
| `ACME_OPENSHIFT*_KEY`         | string | yes      | Secrets for the OpenShift service accounts to update the routes                                                  |
| `OPENSHIFT*_PROJECT`          | string | yes      | OpenShift API endpoints for the OpenShift service accounts                                                       |
| `OPENSHIFT*_SERVER`           | string | yes      | OpenShift projects for the OpenShift service accounts                                                            |
| `ACME_SSH_*_HOST`             | string | yes      | user@host for the ssh-able hosts where certs should be updated                                                   |
| `ACME_SSH_*_CERTIFICATE_NAME` | string | yes      | certificate name for the ssh-able hosts                                                                          |
| `ACME_SSH_*_PRIVATE_KEY_PATH` | string | yes      | full path to the secret key for the ssh-able hosts                                                               |
| `ACME_SSH_*_CERTIFICATE_PATH` | string | yes      | full path to the full certificate chain for the ssh-able hosts                                                   |
| `ACME_SSH_*_COMMAND`          | string | no       | command to run after certificate updates for the ssh-able hosts                                                  |
| `*_SSH_PRIVATE_KEY`           | string | yes      | Secret key for the ssh-able hosts                                                                                |

The AWS service account needs permissions equivalent to the following IAM policy:

```yaml
Version: '2012-10-17'
Statement:
  - Effect: Allow
    Action:
      - route53:ListHostedZonesByName
    Resource: '*'
  - Effect: Allow
    Action:
      - route53:ChangeResourceRecordSets
      - route53:ListResourceRecordSets
    Resource: arn:aws:route53:::hostedzone/ZONE-ID
```

The OpenShift service accounts need permissions equivalent to the following Role:

```yaml
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: acme-deployer
  labels:
    app: acme-deployer
rules:
  - apiGroups: [route.openshift.io]
    resources: [routes]
    verbs: [get, list, patch]
  - apiGroups: [route.openshift.io]
    resources: [routes/custom-host]
    verbs: [create]
```

[certificate alias]: https://github.com/dehydrated-io/dehydrated/blob/master/docs/domains_txt.md#wildcards

## `beaker_tools.respin`

```shell
python3 -m cki.beaker_tools.respin [options]
```

See the command line help for details.

## `brew_trigger`

```shell
python3 -m pipeline_tools.brew_trigger [-c CONFIG] [options] task_id
```

Trigger a CKI pipeline for a kernel from Brew/Koji. The same configuration
files as for the [brew-trigger pipeline-trigger module] can be used. If
`IS_PRODUCTION` variable is not present or set to `False`, retriggered pipelines
are created instead of production.

[brew-trigger pipeline-trigger module]: https://gitlab.com/cki-project/pipeline-trigger/-/blob/master/triggers/brew_trigger.py

## `cki.monitoring_tools.aws_cost_explorer`

Get a short summary of AWS costs.

```shell
Usage: python3 -m cki.monitoring_tools.aws_cost_explorer
    [--threshold COST]
    [--max-items COUNT]
    [--item-cutoff COST]
    [--filter FILTER...]
    [GROUP]
```

The following groups are available:

| Group           | Description                                         |
|-----------------|-----------------------------------------------------|
| `daily`         | Daily costs for the last week, most recent first    |
| `component`     | normalized 7-day average per `ServiceComponent` tag |
| `owner`         | normalized 7-day average per `ServiceOwner` tag     |
| `service`       | normalized 7-day average per AWS Service            |
| `usage_type`    | normalized 7-day average per AWS Usage Type         |
| `instance_type` | normalized 7-day average per AWS Instance Type      |
| `operation`     | normalized 7-day average per AWS API Operation      |

The exit code will be non-zero if the threshold set with `--threshold` is
exceeded for the previous day.

Filters specified with `--filter` can be used to limit the results to certain
tags, e.g.

```shell
python3 -m cki.monitoring_tools.aws_cost_explorer \
    --filter ServiceOwner=CkiProject \
    component
```

The number of shown items can be further limited by cost (`--item-cutoff`) or
count (`--max-items`).

## `cki.monitoring_tools.check_s3_bucket_size`

Summarize size and number of files on an S3 bucket.

```shell
Usage: python3 -m cki.monitoring_tools.check_s3_bucket_size
    [--quota SIZE]
    [--threshold SIZE]
    [--ignore-prefix]
    [BUCKET_SPEC]
```

The Bucket specification needs to be given as the name of an environment variable like

```
BUCKET_SOFTWARE="http://localhost:9000|cki_temporary|super_secret|software|subpath/"
```

The exit code will be non-zero if the total size of all objects in the bucket
(in GB) exceeds the given threshold.

When a quota (in GB) is specified, the output includes the relative usage.

## `cki.deployment_tools.gitlab_runner_config`

This module is used to ensure a managed and reproducible gitlab-runner setup.

### CLI interface

```shell
python3 -m cki.deployment_tools.gitlab_runner_config \
    [-c CONFIG] [options] OBJECT ACTION
```

A gitlab-runner setup can be quite nicely split into the following distinct
objects that can be managed separately:

- `configurations`: gitlab-runner configurations, i.e. the `config.toml` files
  used to configure the gitlab-runner processes, optionally embedded in a
  Kubernetes ConfigMap
- `registrations`: gitlab-runner registrations with GitLab projects or groups
- `variables`: CI/CD variables for GitLab projects or groups
- `webhooks`: webhooks for GitLab projects

The following actions can be performed on them:

- `dump`: get the currently deployed setup, and output it in YAML format
- `generate`: generate the new setup from the configuration, and output it in
  YAML format
- `diff`: show the difference between the currently deployed and the newly
  generated setup
- `apply`: adjust the deployed setup to match the configuration

To register new runners with a GitLab instance, the `--create-missing`
parameter must be specified. The printed runner authorization token must be
added to the `runner_tokens` field of the configuration file. It is considered
a secret and cannot be obtained from the GitLab instance later on!

### Configuration file

The desired gitlab-runner setup is described by a YAML configuration file.
In the following sections, the configuration sections are described based on a
[non-trivial example](gitlab-runner-example.yaml).

The example describes a gitlab-runner setup across two GitLab instances,
`gitlab.com` and `gitlab.corp.com`. On the first (public) instance, all
projects in the `external-group` group should have access to a set of gitlab
runners. On the second (internal) instance, all projects in the
`internal-group` group should have access to a set of identically configured
gitlab runners. Three runners should be configured: two in an OpenShift 3
project (high and normal), and one privileged docker runner for building
container images. There is one project `project1` in the internal group that
needs a slightly different setup. For this project, the OpenShift runners
should be deployed into a different OpenShift 4 project. Additionally, this
project should have access to a password via a CI/CD variable.

#### `runner_tokens`

This dictionary contains the runner authentication tokens indexed by the
generated full name.

Example:

```yaml
runner_tokens:
  com-ocp3-kubernetes-high: $COM_OCP3_KUBERNETES_HIGH_RUNNER_TOKEN
  com-ocp3-kubernetes-normal: $COM_OCP3_KUBERNETES_NORMAL_RUNNER_TOKEN
  com-server-docker-priv: $COM_SERVER_DOCKER_PRIV_RUNNER_TOKEN
  corp-ocp3-kubernetes-high: $CORP_OCP3_KUBERNETES_HIGH_RUNNER_TOKEN
  corp-ocp3-kubernetes-normal: $CORP_OCP3_KUBERNETES_NORMAL_RUNNER_TOKEN
  corp-server-docker-priv: $CORP_SERVER_DOCKER_PRIV_RUNNER_TOKEN
  project1-ocp4-kubernetes-normal: $PROJECT1_OCP4_KUBERNETES_NOR_RUNNER_TOKENMAL
  project1-server-docker-priv: $PROJECT1_SERVER_DOCKER_PRIV_RUNNER_TOKEN
```

Instead of storing those tokens one-by-one in an environment variable or inline, the complete dictionary can be stored in one environment variable in YAML format:

```yaml
runner_tokens: $yaml:GITLAB_RUNNER_TOKENS
```

#### `gitlab_instances`

This dictionary contains general information and access tokens for the
configured GitLab instances.

The `key` is an abbreviation for the instance that is used in other places in
the configuration and in the automatically generated names of the runners.

The `value` is a dictionary with the following fields:

| Field                 | Type   | Required | Description                                                                        |
|-----------------------|--------|----------|------------------------------------------------------------------------------------|
| `url`                 | string | yes      | GitLab instance URL                                                                |
| `api_token`           | string | yes      | GitLab personal access token with `owner` access on the configured projects/groups |
| `registration_tokens` | dict   | yes      | GitLab runner registration tokens for groups and projects                          |

Example:

```yaml
gitlab_instances:
  com:
    url: https://gitlab.com/
    api_token: $COM_GITLAB_TOKEN_ADMIN_BOT
    registration_tokens:
      external-group: $COM_GITLAB_REGISTRATION_TOKEN
  corp:
    url: https://gitlab.corp.com/
    api_token: $CORP_GITLAB_TOKEN_ADMIN_BOT
    registration_tokens:
      internal-group: $CORP_GITLAB_REGISTRATION_TOKEN
      internal-group/project1: $CORP_GITLAB_REGISTRATION_TOKEN_PROJECT1
```

#### `runner_deployments`

This dictionary contains information about configured gitlab-runner
deployments. One deployment corresponds to one `config.toml`, i.e. one
gitlab-runner process. Each gitlab-runner can have multiple configurations
specified in `runner_configurations`.

The `key` is an abbreviation for the deployment that is used in other places in
the configuration and in the automatically generated names of the runners.

The `value` is a dictionary used for templating the global keys of the
`config.toml` file.

Additionally, it can contain the following fields:

| Field                 | Type     | Required | Description                                                                                                  |
|-----------------------|----------|----------|--------------------------------------------------------------------------------------------------------------|
| `.configfile`         | filepath | (yes)    | Filename for the gitlab-runner configuration, e.g. `config.toml` (conflicts with `.configmap`)               |
| `.configmap`          | filepath | (yes)    | Filename for a Kubernetes ConfigMap, e.g. `configmap.yaml` (conflicts with `.configfile`)                    |
| `.filename`           | filename | (yes)    | Filename within the ConfigMap (only with `.configmap`)                                                       |
| `.metadata`           | string   | no       | Meta data for the ConfigMap (only with `.configmap`)                                                         |
| `.template_overrides` | dict     | no       | Runner configuration entries that will only be applied to this deployment                                    |

Example:

```yaml
runner_deployments:
  .default:
    check_interval: 0
    log_level: info
  .kubernetes:
    .filename: config.toml
    .metadata:
      labels:
        app: $PROJECT_NAME
      name: $PROJECT_NAME
    concurrent: 100
  ocp3:
    .extends: .kubernetes
    .configmap: ocp3-configmap.yaml
    .template_overrides:
      kubernetes:
        namespace: $OPENSHIFT_PROJECT
  ocp4:
    .extends: .kubernetes
    .configmap: ocp4-configmap.yaml
    .template_overrides:
      kubernetes:
        namespace: $OPENSHIFT4_PROJECT
  server:
    .configfile: server-config.toml
    concurrent: 10
```

#### `runner_templates`

This dictionary contains templates for gitlab-runner configurations, i.e. for a
`[runner]` section of a `config.toml`. gitlab-runner process.

The `key` is an abbreviation for the runner name that is used in other places in
the configuration and in the automatically generated names of the runners.

The `value` is a dictionary used for templating the keys of a `[runner]`
section of a `config.toml` file.

Additionally, it can contain the following fields:

| Field                                     | Type   | Required | Description                                                                                                                                                        |
|-------------------------------------------|--------|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `description`                             | string | no       | Executor description (only for documentation)                                                                                                                      |
| `rationale`                               | string | no       | Executor rationale (only for documentation)                                                                                                                        |
| `.tags`                                   | dict   | no       | Mapping from tag assignment group to list of gitlab-runner tags                                                                                                    |
| `.cache`                                  | string | no       | Deployment-all style bucket specification for the distributed runner cache in S3                                                                                   |
| `.active`                                 | bool   | no       | Whether the runner is active (default true)                                                                                                                        |
| `.access_level`                           | string | no       | The access_level of the runner; `not_protected` or `ref_protected` (default `not_protected`)                                                                       |
| `.locked`                                 | bool   | no       | Whether the Runner should be locked for current project (default true)                                                                                             |
| `.maximum_timeout`                        | int    | no       | Maximum timeout set when this Runner will handle the job in seconds (default 1 week)                                                                               |
| `.run_untagged`                           | bool   | no       | Whether the Runner should handle untagged jobs (default false)                                                                                                     |
| `machine/.MachineOptions`                 | dict   | no       | Docker machine configuration that gets converted into the key=value array needed for `machine/MachineOptions`                                                      |
| `machine/.MachineOptions/.amazonec2-tags` | dict   | no       | AWS EC2 tags that get converted into the key,value1,key,value2 string needed for `machine/MachineOptions/amazonec2-tags`, can also be provided as YAML/JSON string |

Example:

```yaml
runner_templates:
  .default:
    .cache: $BUCKET_RUNNER_CACHE
  .docker:
    executor: docker
    docker:
      tls_verify: false
  .kubernetes:
    executor: kubernetes
    kubernetes:
      privileged: false
  docker-priv:
    .extends: .docker
    .tags:
      default: [image-builder]
      disabled: []
    docker:
      privileged: true
  kubernetes-high:
    .extends: .kubernetes
    .tags:
      default: [build]
      disabled: []
    limit: 4
    kubernetes:
      cpu_request: "20"
      cpu_limit: "20"
      memory_request: 8Gi
      memory_limit: 8Gi
  kubernetes-normal:
    .extends: .kubernetes
    .tags:
      default: [test]
      disabled: []
    limit: 20
    kubernetes:
      cpu_request: "4"
      cpu_limit: "4"
      memory_request: 4Gi
      memory_limit: 4Gi
```

#### `variable_groups`

This dictionary contains environment variables that can be configured as
trigger variables on a GitLab group or project or in the `environment` key for
a gitlab-runner configuration.

The `key` is an abbreviation for the variable group that is used in other places in
the configuration.

The `value` is a dictionary of environment variables.

Example:

```yaml
variable_groups:
  home:
    HOME: /tmp
  group-default:
    DEFAULT_EMAIL: email@example.com
  project1:
    .extends: group-default
    GITLAB_PASSWORD: $GITLAB_PASSWORD
```

#### `runner_configurations`

This dictionary contains groups of runners that are configured with the same
variables. Those runners can be part of multiple gitlab-runner deployments.

The `key` is an abbreviation for the configuration that is used in other places
and in the automatically generated names of the runners.

The `value` is a dictionary with the following fields:

| Field                | Type   | Required | Description                                                                                   |
|----------------------|--------|----------|-----------------------------------------------------------------------------------------------|
| `runner_deployments` | dict   | yes      | Mapping from runner deployments to list of instantiated runner templates                      |
| `variable_group`     | string | no       | Environment variables set in the `environment` field of the corresponding `[runner]` sections |

Example:

```yaml
runner_configurations:
  com:
    runner_deployments:
      ocp3:
        - kubernetes-high
        - kubernetes-normal
      server:
        - docker-priv
    variable_group: home
  corp:
    runner_deployments:
      ocp3:
        - kubernetes-high
        - kubernetes-normal
      server:
        - docker-priv
    variable_group: home
  project1:
    runner_deployments:
      ocp4:
        - kubernetes-normal
      server:
        - docker-priv
    variable_group: home
```

#### `runner_registrations`

This list contains the registration of runner groups with GitLab groups and
projects. Runners can be part of multiple groups or projects at the same time.

Each item in the list is a dictionary with the following fields:

| Field                   | Type | Required | Description                                                    |
|-------------------------|------|----------|----------------------------------------------------------------|
| `runner_configurations` | list | yes      | Runners that should be linked to the GitLab groups or projects |
| `gitlab_groups`         | list | (yes)    | GitLab groups where runners should be registered               |
| `gitlab_projects`       | list | (yes)    | GitLab projects where runners should be registered             |

Example:

```yaml
runner_registrations:
  - runner_configurations: com
    gitlab_groups: com/external-group
  - runner_configurations: corp
    gitlab_groups: corp/internal-group
  - runner_configurations: project1
    gitlab_projects: corp/internal-group/project1
```

#### `gitlab_variables`

This list contains the assignment of variable groups to GitLab groups and
projects. In other words, this list configures the project/group CI/CD variables.

Each item in the list is a dictionary with the following fields:

| Field             | Type | Required | Description                                                                          |
|-------------------|------|----------|--------------------------------------------------------------------------------------|
| `variable_group`  | list | yes      | Variables that should be set in the CI/CD variables of the GitLab groups or projects |
| `gitlab_groups`   | list | (yes)    | GitLab groups where variables should be set                                          |
| `gitlab_projects` | list | (yes)    | GitLab projects where variables should be set                                        |

Example:

```yaml
gitlab_variables:
  - gitlab_groups:
      - corp/internal-group
      - com/external-group
    variable_group: group-default
  - gitlab_projects:
      - corp/internal-group/project1
    variable_group: project1
```

#### `webhook_endpoints`

This list contains the assignment of webhook endpoints to GitLab projects.

Each item in the list is a dictionary with the following fields:

| Field                     | Type   | Required | Description                                                         |
|---------------------------|--------|----------|---------------------------------------------------------------------|
| `url`                     | string | yes      | The hook URL                                                        |
| `.pattern`                | regex  | no       | Regular expression for the URL to match a hook, defaults to the URL |
| `.secret_token`           | string | yes      | Secret token to validate received payloads                          |
| `.routing_keys`           | list   | yes      | space-separated lists of webhook-receiver routing keys, see below   |
| `.active`                 | bool   | no       | when false, all events are removed, defaults to true                |
| `enable_ssl_verification` | bool   | no       | Do SSL verification when triggering the hook, defaults to true      |

Example:

```yaml
webhook_endpoints:
  - url: https://webhook.endpoint/gitlab
    .pattern: https://(webhook.endpoint|old.endpoint)/gitlab
    .secret_token: $WEBHOOK_SECRET
    .routing_keys:
      - $CONSUMER1_ROUTING_KEYS
      - $CONSUMER2_ROUTING_KEYS
      - gitlab.corp.com.internal-group.project1.build
```

The routing keys are analyzed per GitLab project and webhook endpoint. Only the
referenced events will be enabled.

### Preprocessing

The following preprocessing steps are performed (in order) when reading the configuration.

#### Environment variable substitution

Values of the form `$VARIABLE_NAME` are replaced by the value of the named
environment variable.
Values of the form `$yaml:VARIABLE_NAME` are additionally parsed as YAML.
This allows to store complete dictionaries like the `runner_tokens` in
environment variables.

#### Inheritance

For some dictionaries, basic inheritance and defaults modeled after the
[default](https://docs.gitlab.com/ce/ci/yaml/#global-defaults) and
[extends](https://docs.gitlab.com/ce/ci/yaml/#extends) keywords in
the [GitLab CI/CD job descriptions](https://docs.gitlab.com/ce/ci/yaml) can be
used.

- items with a key starting with a dot (`.`) are not processed, but can be
  used for inheritance
- the `.default` item will be inherited by all other items
- the `.extends` key followed by a name or list of names
  specifies items to inherit from
- if the items are directly used for templating, items with a key starting with
  a dot (`.`) are removed beforehand

This is enabled for `runner_templates`, `runner_deployments` and
`variable_groups`.

## `cki.cki_tools.webhook_receiver`

The webhook receiver can post events received from GitLab and Sentry.io to an
AMQP message bus.

| Environment variable                       | Description                           |
|--------------------------------------------|---------------------------------------|
| `RABBITMQ_HOST`                            | AMQP host                             |
| `RABBITMQ_PORT`                            | AMQP port, TLS is used for port 443   |
| `RABBITMQ_USER`                            | AMQP user                             |
| `RABBITMQ_PASSWORD`                        | AMQP password                         |
| `RABBITMQ_EXCHANGE`                        | AMQP exchange for the posted messages |
| `WEBHOOK_RECEIVER_WEBSECRET`               | GitLab webhook secret                 |
| `WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET` | Sentry.io webhook client secret       |

### GitLab integration

Go to <https://gitlab.com/your-project/hooks>, and add a new webhook for
<https://webhook-receiver-host/>, the secret from the `WEBHOOK_RECEIVER_WEBSECRET`
environment variable and all required trigger types.

Messages posted to the exchange will use a routing key of
`hostname.project.event`, e.g.
`gitlab.com.cki-project.kernel-ark.merge_request`.

### Sentry.io integration

Go to Settings -> Organization -> Developer Settings, create a new internal
integration with a webhook URL of <https://webhook-receiver-host/sentry>, and
enable `Alert Rule Action` there. Save the client secret in the
`WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET` environment variable.

For each project where events should be forwarded to the message bus, go to the
Alerts page and Create an alert rule with `Issue Alert`, when `an event is
seen` and `send a notification via an integration` via the integration defined
above.

Messages posted to the exchange will use a routing key of
`sentry.io.project.resource.action`, e.g.
`sentry.io.webhook-receiver.event_alert.triggered`.

## KCIDB Image.

An extra image is built to run the KCIDB module.
This image is used on OpenShift pods, and contains `kcidb` extra dependencies.

`registry.gitlab.com/cki-project/cki-tools/kcidb`

## `cki.deployment_tools.grafana`

```shell
python3 -m cki.deployment_tools.grafana download|upload [--path PATH]
```

Tool to backup and restore Grafana data. Download configuration into locally stored
JSON files, which can be later be modified and re uploaded.

- `download` option uses Grafana API to download datasources, notification channels and
dashboards and saves them to PATH.

- `upload` takes the json files stored in PATH and restores them to the Grafana server.

`GRAFANA_HOST` and `GRAFANA_TOKEN` environment variables need to be configured.
Follow the instructions on the [Grafana Docs] to learn how to get an API Token.

[Grafana Docs]: https://grafana.com/docs/grafana/latest/http_api/auth/#create-api-token

## `cki.deployment_tools.scheduler`

### Purpose

The `scheduler` is a deployment helper for recurring jobs that run on a
schedule. Such a schedule can be deployed as an OpenShift `CronJob` or a GitLab
CI/CD pipeline schedule.

### Creating a schedule

The [example schedule](cki/deployment_tools/scheduler/schedule-example.yml)
should give an idea what a schedule looks like.

| Field       | Type   | Required | Description                                                          |
|-------------|--------|----------|----------------------------------------------------------------------|
| `name`      | string | yes      | schedule name, used to build names for deployed resources            |
| `image`     | string | yes      | container image URL including tag                                    |
| `secrets`   | array  | no       | secret [environment variables] to expose to all jobs in the schedule |
| `variables` | array  | no       | [environment variables] to expose to all jobs in the schedule        |
| `jobs`      | dict   | no       | [scheduled jobs]                                                     |

[scheduled jobs]: #jobs
[environment variables]: #environment-variables

#### Jobs

The following fields describe the jobs to run:

| Field        | Type         | Required | Description                                                                |
|--------------|--------------|----------|----------------------------------------------------------------------------|
| `schedule`   | string       | yes      | cron-style schedule for the job (e.g. `0 1 * * *`) ([Cron syntax])         |
| `command`    | array        | (yes)    | Bash command line to run (cannot be combined with `module`)                |
| `module`     | string/array | (yes)    | Python module and optional args to run (cannot be combined with `command`) |
| `image`      | string       | no       | container image URL including tag, overrides schedule setting              |
| `variables`  | array        | no       | [environment variables] to expose to this job                              |
| `disabled`   | bool         | no       | ignore this job during deployment                                          |
| `kubernetes` | dict         | no       | [Kubernetes-specific job configuration]                                    |
| `gitlab`     | dict         | no       | [GitLab-specific job configuration]                                        |
| `config`     | dict         | no       | inline yaml configuration, [environment variables] are replaced            |

[Cron syntax]: https://en.wikipedia.org/wiki/Cron
[Kubernetes-specific job configuration]: #kubernetes-specific-job-configuration
[GitLab-specific job configuration]: #gitlab-specific-job-configuration

#### Environment variables

Each job can be customized by specifying common secrets and variables, and
per-job variables. Secrets are stored as masked CI/CD variables (GitLab) or
Kubernetes Secrets (OpenShift).

Both are configured as arrays in one of the following formats:

| Item          | Variable name | Variable value                                        |
|---------------|---------------|-------------------------------------------------------|
| - KEY         | `KEY`         | value of `KEY` environment variable during deployment |
| - KEY: $VAR   | `KEY`         | value of `VAR` environment variable during deployment |
| - KEY: INLINE | `KEY`         | inline value of `INLINE` (not supported for secrets)  |

#### Kubernetes-specific job configuration

The `kubernetes` field for a job contains configuration only applicable to a
Kubernetes/OpenShift deployment:

| Field                   | Type   | Required | Description                                                                            |
|-------------------------|--------|----------|----------------------------------------------------------------------------------------|
| `memory`                | string | no       | [Kubernetes memory limits and requests]                                                |
| `cpu`                   | float  | no       | [Kubernetes CPU limits and requests], defaults to 1                                    |
| `concurrencyPolicy`     | string | no       | [Kubernetes Concurrency Policy], defaults to `Replace`                                 |
| `activeDeadlineSeconds` | int    | no       | [Kubernetes Init Containers], on the Pod template, defaults to empty                   |
| `serviceAccountName`    | string | no       | [Kubernetes Service Accounts], on the Pod template, defaults to empty                  |
| `dataVolume`            | string | no       | [Kubernetes Volume], `emptyDir` (default), `memory` or name of `persistentVolumeClaim` |
| `labels`                | dict   | no       | [Kubernetes Labels], additional to `app` and `schedule_job`                            |

[Kubernetes memory limits and requests]: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#meaning-of-memory
[Kubernetes CPU limits and requests]: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#meaning-of-cpu
[Kubernetes Concurrency Policy]: https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/#concurrency-policy
[Kubernetes Init Containers]: https://kubernetes.io/docs/concepts/workloads/pods/init-containers/#detailed-behavior
[Kubernetes Service Accounts]: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
[Kubernetes Volume]: https://kubernetes.io/docs/concepts/storage/volumes/
[Kubernetes Labels]: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/

#### GitLab-specific job configuration

The `gitlab` field for a job contains configuration only applicable to a
GitLab deployment:

| Field  | Type  | Required | Description          |
|--------|-------|----------|----------------------|
| `tags` | array | no       | [GitLab runner tags] |

[GitLab runner tags]: https://docs.gitlab.com/ce/ci/yaml/#tags

### Deploying a schedule

Make sure all secrets in the schedule are defined as environment variables, and
then call

```shell
python3 -m cki.deployment_tools.scheduler -c schedule.yml --environment ENVIRONMENT
```

The following environments are supported:

#### OpenShift - `openshift`

Deploys a complete schedule as an OpenShift `CronJob`. All resources are
labeled with `app=scheduler-SCHEDULE` and
`schedule_job=scheduler-SCHEDULE-JOB`. The secrets are deployed as one
common `Secret` resource that is referenced by the individual jobs.

The following additional parameters are supported:

- `--no-cleanup`: optional, do not remove old jobs that are not in the schedule
  anymore
- `--disabled`: optional, disable all jobs independent of the `disabled` settings
  of the schedule

#### GitLab - `gitlab`

Deploys a complete schedule as GitLab CI/CD pipeline schedules. The project is
created/reconfigured if needed. The secrets are deployed into masked (if
possible) project variables. The job variables are deployed into schedule
variables. The `.gitlab-ci.yml` CI/CD pipeline configuration is rewritten to
contain one job per schedule.

The following additional parameters are supported:

- `--schedule-project-url`: required, full URL of the GitLab project that will
  hold the CI/CD pipeline schedules
- `--no-cleanup`: optional, do not remove GitLab CI/CD pipeline schedules that
  are not in the schedule anymore

The GitLab personal access token of the owner of the CI/CD pipeline schedules
needs to be provided in the `GITLAB_TOKENS` environment variable:

```shell
export GITLAB_TOKENS='{"gitlab.com": "COM_GITLAB_TOKEN"}'
export COM_GITLAB_TOKEN='0123456789abcdef'
```

#### Podman - `podman`

Runs one job of a schedule immediately with the help of Podman.

The following additional parameters are supported:

- `--job JOB`: required, specifies the job to run immediately
- `--code-overlay DIR`: optional, mount a local directory at /code
- `--image IMAGE`: optional, override the image from the schedule

The job is run in a read-only Podman container with a `tmpfs` in `/data`.

#### Local shell - `local`

Runs one job of a schedule immediately in the current shell environment.

The following additional parameters are supported:

- `--job JOB`: required, specifies the job to run immediately

### Development

#### Environment variables provided to the bots

- `SCHEDULER_CONFIG`: JSON job configuration from the schedule
- `SCHEDULER_DATA_DIR`: writable temporary directory
- `SCHEDULER_JOB_NAME`: name of the scheduled job

#### Running a job locally

A job can be run immediately on the local machine in two environments: in the
current shell environment (`local`) or in a Podman container (`podman`).

As an example, this would launch the `git-cache-updater job` of `schedule.yml`
in a Podman container, running code from the current working directory and
using the `cki-tools` image from the local host:

```shell
python3 -m cki.deployment_tools.scheduler -c schedule.yml \
    --environment podman \
    --job git-cache-updater \
    --code-overlay . \
    --image localhost/cki-tools
```

#### OpenShift

To manually run a job for a schedule, debug the `CronJob` and run the printed
command manually via

```shell
oc debug cronjob/scheduler-SCHEDULE-JOB
```

To delete everything, run

```shell
oc delete all -l app=scheduler
```

#### Render jinja2 templates

Deployment tools contain a script to render files using jinja2 templating.

```shell
$ python3 -m cki.deployment_tools.render_jinja2 --help
usage: render_jinja2.py [-h] [-o OUTPUT] template data

Render a template with jinja2

positional arguments:
  template              Template file to render.
  data                  Input data file for the template.

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Output path for the rendered file.
```

Besides normal jinja2 templating, an `env` filter is provided.
Using `env` allows to replace variables following the bash notation (`${VAR_NAME}` or `$VAR_NAME`) with environment variables.

If a variable is defined on the data file as `foo: ${BAR}`, and it's used with the `env` filter `{{ foo|env }}` it will be replaced with the content of the `BAR` env var.
If `BAR` is not defined, a `KeyError` exception will be raised.

To use environment variables on the template, the notation needs to match the following format: `{{ env["VAR_NAME"] }}`.
