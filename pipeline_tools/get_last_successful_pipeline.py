import copy

from cki_lib import cki_pipeline


def main(project, args):
    variable_filter = copy.copy(args.variable_filter)
    if args.cki_pipeline_type:
        variable_filter['cki_pipeline_type'] = args.cki_pipeline_type
    return cki_pipeline.last_successful_pipeline_for_branch(
        project, args.cki_pipeline_branch,
        variable_filter=variable_filter)
