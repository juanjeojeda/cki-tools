"""Retrigger pipelines already run."""
from cki_lib import cki_pipeline
from cki_lib import misc
from cki_lib.logger import get_logger

LOGGER = get_logger(__name__)


def main(gl_project, args):
    """Retrigger a pipline."""
    gl_pipeline = cki_pipeline.retrigger(
        gl_project, args.trigger_token, args.pipeline_id,
        pipeline_definition_repository=args.pipeline_definition_repository,
        pipeline_definition_branch=args.pipeline_definition_branch,
        tree_repository=args.tree_repository,
        tree_branch=args.tree_branch,
        tests_only=args.tests_only,
        variable_overrides=args.variables,
        is_production=misc.is_production(),
        interactive=True)
    print(f'Pipeline: {gl_pipeline.web_url}')
