"""Route webhook messages to an amqp queue (AWS Lambda version)."""
import os

from cki_lib import misc
import sentry_sdk
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration

from cki.cki_tools.webhook_receiver import receiver

# ^ absolute import as AWS Lambda does not use python -m

if misc.is_production():
    sentry_sdk.init(
        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
        integrations=[AwsLambdaIntegration()]
    )

receiver.TRY_ENDLESSLY = True


def _lambda_handler(method, event):
    status_code, message = method(event['headers'],
                                  event['body'].encode('utf8'))
    return {'statusCode': status_code, 'body': message}


def gitlab_lambda(event, _):
    """Process a webhook."""
    return _lambda_handler(receiver.gitlab_handler, event)


def sentry_lambda(event, _):
    """Process a webhook."""
    return _lambda_handler(receiver.sentry_handler, event)
