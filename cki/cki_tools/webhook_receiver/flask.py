"""Route webhook messages to an amqp queue (Flask version)."""
import os

from cki_lib import misc
import flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from . import receiver

app = flask.Flask(__name__)

if misc.is_production():
    sentry_sdk.init(
        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
        integrations=[FlaskIntegration()]
    )

receiver.TRY_ENDLESSLY = False


def _flask_handler(method):
    status_code, message = method(flask.request.headers,
                                  flask.request.get_data())
    if status_code != 200:
        flask.abort(status_code, message)
    return message


@app.route('/', methods=['POST'])
def gitlab_webhook():
    """Process a webhook."""
    return _flask_handler(receiver.gitlab_handler)


@app.route('/sentry', methods=['POST'])
def sentry_webhook():
    """Process a webhook."""
    return _flask_handler(receiver.sentry_handler)
