#!/bin/bash
set -euo pipefail

# Check the connectivity to a git repo
#
# Usage: git-ls-remote-check.sh REPO_URL

git ls-remote "$1" 2>&1 > /dev/null
