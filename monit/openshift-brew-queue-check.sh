#!/bin/bash
set -euo pipefail

# Check the queue size from the brew trigger
#
# Usage: openshift-brew-queue-check.sh BREW_NAME MAX_QUEUE_SIZE PREFIX
#
# Required environment variables:
# - ${PREFIX}_KEY
# - ${PREFIX}_SERVER
# - ${PREFIX}_PROJECT

BREW_NAME=$1 MAX_QUEUE_SIZE=$2 PREFIX=${3:-OPENSHIFT}
KEY_NAME=${PREFIX}_KEY
SERVER_NAME=${PREFIX}_SERVER
PROJECT_NAME=${PREFIX}_PROJECT

# Get the last logged queue size
LAST_QUEUE_SIZE="$(curl -k -s -H "Authorization: Bearer ${!KEY_NAME}" "${!SERVER_NAME}/apis/apps.openshift.io/v1/namespaces/${!PROJECT_NAME}/deploymentconfigs/$BREW_NAME/log?sinceSeconds=120" | sed -En 's/.*queue_size=([0-9]+).*/\1/p' | tail -n 1)"

echo "queue_size=$LAST_QUEUE_SIZE"

test "$LAST_QUEUE_SIZE" -le "$MAX_QUEUE_SIZE"
