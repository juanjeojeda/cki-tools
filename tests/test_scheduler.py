"""Deployment unit tests."""
import json
import os
import unittest
from unittest import mock

from cki.deployment_tools.scheduler import deployment
from cki.deployment_tools.scheduler import openshift

SCHEDULER = 'cki.deployment_tools.scheduler'


class TestDeployment(unittest.TestCase):
    """Test the deployment via the scheduler."""
    @mock.patch.dict(os.environ, {'key1': 'value1', 'key2': 'value2'})
    def test_resolve_variables(self):
        """Check nested config variable replacements."""
        data = {
            'key': 'value',
            'key1': '$key1',
            'key3': {'key4': {'key2': '$key2', 'key5': 'value5'}},
            'key6': ['$key2'],
            'key7': '${key1}',
        }
        deployment.Deployment({}, {}).resolve_variables(data)
        self.assertEqual(data, {
            'key': 'value',
            'key1': 'value1',
            'key3': {'key4': {'key2': 'value2', 'key5': 'value5'}},
            'key6': ['value2'],
            'key7': 'value1',
        })

    @mock.patch(f'{SCHEDULER}.openshift.OpenShiftDeployment._run')
    def test_kubernetes_suspend_true(self, run):
        """Test suspending jobs."""
        self._test_kubernetes_suspend(run, True, cmdline=True)

    @mock.patch(f'{SCHEDULER}.openshift.OpenShiftDeployment._run')
    def test_kubernetes_suspend_none(self, run):
        """Test unsuspending jobs."""
        self._test_kubernetes_suspend(run, False)

    def _test_kubernetes_suspend(self, run, expected, cmdline=None):
        openshift.OpenShiftDeployment({
            'name': 'unit-test',
            'image': 'image',
            'jobs': {'suspend': {'module': 'module', 'schedule': 'schedule'}},
        }, {}, cmdline).deploy()
        cronjob = json.loads(run.call_args.kwargs['input'])['items'][1]
        self.assertIs(cronjob['spec']['suspend'], expected)

    @mock.patch(f'{SCHEDULER}.openshift.OpenShiftDeployment._run')
    def test_kubernetes_service_account_none(self, run):
        """Test unspecified service accounts."""
        self._test_kubernetes_service_account(run, None)

    @mock.patch(f'{SCHEDULER}.openshift.OpenShiftDeployment._run')
    def test_kubernetes_service_account_set(self, run):
        """Test passing service accounts."""
        self._test_kubernetes_service_account(run, 'account')

    def _test_kubernetes_service_account(self, run, account):
        job = {'module': 'module', 'schedule': 'schedule'}
        if account:
            job.setdefault('kubernetes', {})['serviceAccountName'] = account
        openshift.OpenShiftDeployment({
            'name': 'unit-test',
            'image': 'image',
            'jobs': {'default': job},
        }, {}, None).deploy()
        cronjob = json.loads(run.call_args.kwargs['input'])['items'][1]
        pod_spec = cronjob['spec']['jobTemplate']['spec']['template']['spec']
        if account:
            self.assertEqual(pod_spec['serviceAccountName'], account)
        else:
            self.assertNotIn('serviceAccountName', pod_spec)

    @mock.patch(f'{SCHEDULER}.openshift.OpenShiftDeployment._run')
    def test_module_no_args(self, run):
        """Test the handling of modules without args."""
        openshift.OpenShiftDeployment({
            'name': 'unit-test',
            'image': 'image',
            'jobs': {'job': {'module': 'module', 'schedule': 'schedule'}},
        }, {}, None).deploy()
        cronjob = json.loads(run.call_args.kwargs['input'])['items'][1]
        spec = cronjob['spec']['jobTemplate']['spec']['template']['spec']
        self.assertEqual(spec['containers'][0]['command'],
                         ['python3', '-m', 'module'])

    @mock.patch(f'{SCHEDULER}.openshift.OpenShiftDeployment._run')
    def test_module_args(self, run):
        """Test the handling of modules with args."""
        openshift.OpenShiftDeployment({
            'name': 'unit-test',
            'image': 'image',
            'jobs': {'job': {'module': ['module', 'arg'],
                             'schedule': 'schedule'}},
        }, {}, None).deploy()
        cronjob = json.loads(run.call_args.kwargs['input'])['items'][1]
        spec = cronjob['spec']['jobTemplate']['spec']['template']['spec']
        self.assertEqual(spec['containers'][0]['command'],
                         ['python3', '-m', 'module', 'arg'])
